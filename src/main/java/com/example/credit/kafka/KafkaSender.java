package com.example.credit.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class KafkaSender {
	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;
 
	public void sendMessage(Object tradeData) {
 
		Message<Object> audit = MessageBuilder.withPayload(tradeData).setHeader(KafkaHeaders.TOPIC, "audittopic").build();
		kafkaTemplate.send(audit);
		log.info(String.format("messeage sent %s", audit));
}
}
