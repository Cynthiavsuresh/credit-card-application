package com.example.credit.entity;

import java.math.BigDecimal;

import com.example.credit.dto.CardType;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Credit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long creditId;
	private Long creditScore;
	private Long creditCardNum;
	@Enumerated(EnumType.STRING)
	private CardType cardType;
	private BigDecimal cardLimit;
	private Long cardPin;
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	

}
	
