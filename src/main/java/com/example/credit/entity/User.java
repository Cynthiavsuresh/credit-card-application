package com.example.credit.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.example.credit.dto.IdentityType;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	private String name;
	private String email;
	private int age;
	private LocalDate dob;
	@Enumerated(EnumType.STRING)
	private IdentityType identityType;
	private String identitynumber;
	private String occupation;
	private BigDecimal salary;
	
}
