package com.example.credit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.credit.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
