package com.example.credit.dto;

public enum Status {
	PENDING, REJECT, APPROVED
}
