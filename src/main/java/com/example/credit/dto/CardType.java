package com.example.credit.dto;

public enum CardType {
	GOLD, PLATINUM, VISA
}
